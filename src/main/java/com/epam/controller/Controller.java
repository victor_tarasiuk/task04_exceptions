package com.epam.controller;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public interface Controller {

    /**
     * Will generate default file.
     */
     void generateCreateDefaultFile();

    /**
     * Will generate default file with name.
     */
    void generateCreateFileWithName(String name);

}
