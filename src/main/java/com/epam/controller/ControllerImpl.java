package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    /**
     * Created method generate default file.
     */
    @Override
    public final void generateCreateDefaultFile() {

         model.createDefaultFile();
    }

    /**
     * Created method generate default file with name.
     */
    @Override
    public final void generateCreateFileWithName(String name) {

        model.createFileWithName(name);
    }


}
