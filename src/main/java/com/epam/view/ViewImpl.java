package com.epam.view;

import com.epam.controller.ControllerImpl;
import java.util.Scanner;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class ViewImpl implements View {
    /**
     * Creates object ControllerImpl to execute methods.
     */
    ControllerImpl controller = new ControllerImpl();

    /**
     * Created Scanner to set numbers.
     */
    Scanner scanner = new Scanner(System.in);

    /**
     * Created method to show what user should do and display information.
     */
    public final void show() {

        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Create default file : 1");
            System.out.println("Create file with name : 2");

            String string = scanner.nextLine();

            switch (string) {

                case "1":
                    controller.generateCreateDefaultFile();
                    break;

                case "2":
                    System.out.println("Enter file name: ");
                    String name = scanner.nextLine();
                    controller.generateCreateFileWithName(name);
                    break;

                default: return;
            }
        }

    }

}
