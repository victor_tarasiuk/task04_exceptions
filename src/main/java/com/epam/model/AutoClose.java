package com.epam.model;

import java.io.*;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class AutoClose implements AutoCloseable {

    @Override
    public void close()  throws ExceptionClose {
        throw new ExceptionClose();
    }

    /**
     * Created method create default file with name.
     */
    public void auto(String name) {
        try {
            PrintWriter printWriter = new PrintWriter(name+ ".txt");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}

