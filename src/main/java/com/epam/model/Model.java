package com.epam.model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public interface Model {

    /**
     * Will get default file.
     */
    void createDefaultFile();

    /**
     * Will get default file with name.
     */
    void createFileWithName(String name);

}
