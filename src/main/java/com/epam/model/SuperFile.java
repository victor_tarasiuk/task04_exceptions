package com.epam.model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class SuperFile {
    PrintWriter pw = null;

    public SuperFile() {

    }

    /**
     * Created method create default file.
     */
    void defaultFile() throws ExceptionDefaultFile {
        try {
            pw = new PrintWriter("DefaultName.txt");
            throw new ExceptionDefaultFile();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } finally {
            pw.close();
        }
    }

    /**
     * Created method generate creation default file with name.
     */
    void fileWithName(String name) {
        try(AutoClose autoClose = new AutoClose()) {
            autoClose.auto(name);

        } catch (ExceptionClose exceptionClose){
            exceptionClose.printStackTrace();
        }
    }
}
