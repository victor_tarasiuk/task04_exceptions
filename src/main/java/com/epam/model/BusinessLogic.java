package com.epam.model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class BusinessLogic implements Model {

    SuperFile superFile;

    public BusinessLogic() {
        superFile = new SuperFile();
    }

    /**
     * Created method generate get default file.
     */
    @Override
    public final void createDefaultFile() {
        try {
            superFile.defaultFile();

        } catch (ExceptionDefaultFile exceptionDefaultFile) {
            exceptionDefaultFile.printStackTrace();
        }
    }

    /**
     * Created method generate default file with name.
     */
    @Override
    public final void createFileWithName(String name) {

        superFile.fileWithName(name);
    }
}