package com.epam.model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class ExceptionClose extends RuntimeException {
    public ExceptionClose(){

        super("Created File With Name");
    }
}
