package com.epam.model;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 20.11.2019
 *
 */
public class ExceptionDefaultFile extends Exception {
    public ExceptionDefaultFile(){

        super("Created Default File");
    }
}
